# Desafio 01 - Formação DevOps - Cloud Treinamentos

### Etapas:
1. Criar Dockerfile versionado no GitLab
1. Criar Job no jenkins para fazer o Build 
1. Fazer o envio da imagem latest para o Dockerhub

[Dockerhub](https://hub.docker.com/repository/docker/rodrigoturatti/formacao-devops-weather)
